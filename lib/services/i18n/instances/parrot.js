'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = factory;

var _parrot = require('../../../request/instances/parrot');

var _parrot2 = _interopRequireDefault(_parrot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function factory() {
  var baseUrl = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/api';

  return {

    fetchSupportedLanguages: function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt('return', (0, _parrot2.default)(baseUrl + '/languages').then(function (res) {
                  if (res && res.data) {
                    // => res.data = [
                    //   {
                    //     "name": "中文简体",
                    //     "locale": "zh-Hans"
                    //   }
                    // ]
                    res.data = res.data.map(function (src) {
                      return {
                        name: src.name,
                        lang: src.lag || src.lang
                      };
                    });
                    // <= res.data = [
                    //   {
                    //     "name": "中文简体",
                    //     "lang": "zh-Hans"
                    //   }
                    // ]
                  }
                  return res;
                }));

              case 1:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function fetchSupportedLanguages() {
        return _ref.apply(this, arguments);
      }

      return fetchSupportedLanguages;
    }(),

    fetchWordsMap: function () {
      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(lang) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt('return', (0, _parrot2.default)(baseUrl + '/words/listAll', { body: { lang: lang } }).then(function (res) {
                  if (res && res.data) {
                    // => res.data = [
                    //   {
                    //     "id": 100045,
                    //     "createdDate": 19299599694,
                    //     "modifiedDate": 19299599694,
                    //     "key": "title",
                    //     "values": [
                    //       {
                    //         "value": "标题",
                    //         "lang": "zh_CN"
                    //       }
                    //     ]
                    //   }
                    // ]
                    var map = {};
                    res.data.forEach(function (src) {
                      map[src.key] = (src.values.filter(function (v) {
                        return v.lang === lang;
                      })[0] || {}).value;
                    });
                    res.data = map;
                    // <= res.data = {
                    //   "key": "文案",
                    //   "key": "文案",
                    //   ...
                    // }
                  }
                  return res;
                }));

              case 1:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function fetchWordsMap(_x2) {
        return _ref2.apply(this, arguments);
      }

      return fetchWordsMap;
    }()

  };
}