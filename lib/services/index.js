'use strict';

var _parrot = require('./i18n/instances/parrot');

var _parrot2 = _interopRequireDefault(_parrot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  parrotI18nService: _parrot2.default
};