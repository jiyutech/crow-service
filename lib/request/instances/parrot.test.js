'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _parrot = require('./parrot');

var _parrot2 = _interopRequireDefault(_parrot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// let APIBase = 'http://test-paradise.jiyutech.net';
var APIBase = 'http://test-mctex.jiyutech.net';
var loginToken = void 0;

test('ParrotRequest should be a function', function () {
  expect(typeof _parrot2.default === 'undefined' ? 'undefined' : _typeof(_parrot2.default)).toBe('function');
});

test('Non-Login returns 401', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
  var res;
  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _parrot2.default)(APIBase + '/accounts', {
            method: 'GET'
          });

        case 2:
          res = _context.sent;

          expect(res.success).toBe(false);
          expect(res.status).toBe(401);

        case 5:
        case 'end':
          return _context.stop();
      }
    }
  }, _callee, undefined);
})));

test('Login failed password error', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
  var res;
  return regeneratorRuntime.wrap(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _parrot2.default)(APIBase + '/logins', {
            method: 'POST',
            body: {
              "username": "system",
              "password": "1"
            }
          });

        case 2:
          res = _context2.sent;

          expect(res.success).toBe(false);
          expect(res.data).toBe(null);
          expect(res.errorType).toBe('biz_error');
          expect(_typeof(res.errorMessage)).toBe('string');
          expect(_typeof(res.errorCode)).toBe('string');
          expect(res.status).toBe(500);

        case 9:
        case 'end':
          return _context2.stop();
      }
    }
  }, _callee2, undefined);
})));

describe('needs login', function () {

  beforeAll(function () {
    return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
      var _ref4, success, data;

      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return (0, _parrot2.default)(APIBase + '/logins', {
                method: 'POST',
                body: {
                  "username": "system",
                  "password": "Hello1234"
                }
              });

            case 2:
              _ref4 = _context3.sent;
              success = _ref4.success;
              data = _ref4.data;

              expect(success).toBe(true);
              expect(_typeof(data.loginToken)).toBe('string');
              loginToken = data.loginToken;

            case 8:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, undefined);
    }))();
  });

  // test('Login success', );

  test('Loged in returns 200', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
    var _ref6, success, data, status;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return (0, _parrot2.default)(APIBase + '/accounts', {
              body: {
                loginToken: loginToken
              }
            });

          case 2:
            _ref6 = _context4.sent;
            success = _ref6.success;
            data = _ref6.data;
            status = _ref6.status;

            expect(success).toBe(true);
            expect(status).toBe(200);
            expect(data.resultSet instanceof Array).toBe(true);
            expect(_typeof(data.currentPage)).toBe('number');
            expect(_typeof(data.totalPages)).toBe('number');
            expect(_typeof(data.pageSize)).toBe('number');
            expect(_typeof(data.totalRecords)).toBe('number');

          case 13:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined);
  })));

  test('204 request should be success', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
    var createRes, deleteRes;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return (0, _parrot2.default)(APIBase + '/productProperties?loginToken=' + loginToken, {
              method: 'POST',
              body: {
                name: Date.now() + '',
                type: 'color',
                common: false
              }
            });

          case 2:
            createRes = _context5.sent;

            expect(createRes.success).toBe(true);
            expect(!!createRes.data.id).toBe(true);
            _context5.next = 7;
            return (0, _parrot2.default)(APIBase + '/productProperties/' + createRes.data.id, {
              method: 'DELETE',
              body: {
                loginToken: loginToken
              }
            });

          case 7:
            deleteRes = _context5.sent;

            expect(deleteRes.status).toBe(204);
            expect(deleteRes.success).toBe(true);

          case 10:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, undefined);
  })));
});