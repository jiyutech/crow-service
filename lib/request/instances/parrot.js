'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.default = request;

var _qs = require('qs');

var _qs2 = _interopRequireDefault(_qs);

var _nodeFetch = require('node-fetch');

var _nodeFetch2 = _interopRequireDefault(_nodeFetch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// /**
//  * Requests a URL, returning a promise.
//  *
//  * @param  {string} url       The URL we want to request
//  * @param  {object} [options] The options we want to pass to "fetch"
//  * @return {object}           An object containing either "data" or "err"
//  */
//
// return {
//   success: boolean,
//   data: core data json,
//   errorType: enums, // TODO network_error, ...
//   errorMessage: string,
//   errorItems: string,
//   errorCode: number,
//   errorDebugInfo: number,
//   status: number,
// }

function request(url) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};


  if (options.params) {
    options.body = options.params;
    delete options.params;
  }

  var defaultOptions = {
    method: 'GET'
  };
  var newOptions = _extends({}, defaultOptions, options);
  newOptions.method = (newOptions.method || '').toUpperCase();
  if (newOptions.method === 'POST' || newOptions.method === 'PUT' || newOptions.method === 'PATCH') {
    newOptions.headers = _extends({
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8'
    }, newOptions.headers);
    newOptions.body = JSON.stringify(newOptions.body);
  } else if (newOptions.body) {
    url += (url.indexOf('?') == -1 ? '?' : '&') + _qs2.default.stringify(newOptions.body);
  }

  return new Promise(function (resolve) {
    var status = void 0,
        rawResponse = void 0;
    (0, _nodeFetch2.default)(url, newOptions).then(function (response) {
      status = response.status;
      rawResponse = response;
      if (status === 204) {
        return response.text();
      }
      return response.json();
    }, function (e) {
      resolve({
        success: false,
        data: null,
        errorType: 'network_error',
        errorMessage: null,
        errorItems: null,
        errorCode: null,
        errorDebugInfo: e,
        status: status,
        response: rawResponse
      });
    }).then(function (res) {
      // TODO: none 2xx Fail catch.
      console.warn(status);

      if (!res && status != 204 || res.errorCode) {
        resolve({
          success: false,
          data: null,
          errorType: res ? 'biz_error' : 'no_content',
          errorMessage: res && res.errorMessage,
          errorItems: res && res.errorItems,
          errorCode: res && res.errorCode,
          errorDebugInfo: res && res.errorDebugInfo,
          status: status,
          response: rawResponse
        });
      } else {
        resolve({
          success: true,
          data: res,
          errorType: null,
          errorMessage: '',
          errorItems: null,
          errorCode: null,
          errorDebugInfo: null,
          status: status,
          response: rawResponse
        });
      }
    }).catch(function (e) {
      resolve({
        success: false,
        data: null,
        errorType: 'json_parse_error',
        errorMessage: '',
        errorItems: null,
        errorCode: null,
        errorDebugInfo: e,
        status: status,
        response: rawResponse
      });
    });
  });
}