'use strict';

var _parrot = require('./instances/parrot');

var _parrot2 = _interopRequireDefault(_parrot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = {
  parrotRequest: _parrot2.default
};