import parrotRequest from './parrot';

// let APIBase = 'http://test-paradise.jiyutech.net';
let APIBase = 'http://test-mctex.jiyutech.net';
let loginToken;


test('ParrotRequest should be a function', () => {
  expect(typeof parrotRequest).toBe('function');
});

test('Non-Login returns 401', async () => {
  let res = await parrotRequest(`${APIBase}/accounts`, {
    method: 'GET',
  })
  expect(res.success).toBe(false);
  expect(res.status).toBe(401);
});

test('Login failed password error', async () => {
  let res = await parrotRequest(`${APIBase}/logins`, {
    method: 'POST',
    body: {
      "username": "system",
      "password": "1"
    }
  });
  expect(res.success).toBe(false);
  expect(res.data).toBe(null);
  expect(res.errorType).toBe('biz_error');
  expect(typeof res.errorMessage).toBe('string');
  expect(typeof res.errorCode).toBe('string')
  expect(res.status).toBe(500);
});


describe('needs login', () => {

  beforeAll(() => {
    return (async () => {
      let {success, data} = await parrotRequest(`${APIBase}/logins`, {
        method: 'POST',
        body: {
          "username": "system",
          "password": "Hello1234"
        }
      });
      expect(success).toBe(true);
      expect(typeof data.loginToken).toBe('string');
      loginToken = data.loginToken;
    })();
  });

  // test('Login success', );

  test('Loged in returns 200', async () => {
    let { success, data, status } = await parrotRequest(`${APIBase}/accounts`, {
      body: {
        loginToken
      }
    });
    expect(success).toBe(true);
    expect(status).toBe(200);
    expect(data.resultSet instanceof Array).toBe(true);
    expect(typeof data.currentPage).toBe('number');
    expect(typeof data.totalPages).toBe('number');
    expect(typeof data.pageSize).toBe('number');
    expect(typeof data.totalRecords).toBe('number');
  });


  test('204 request should be success', async () => {
    let createRes = await parrotRequest(`${APIBase}/productProperties?loginToken=${loginToken}`, {
      method: 'POST',
      body: {
        name: Date.now()+'',
        type: 'color',
        common: false
      }
    });
    expect(createRes.success).toBe(true);
    expect(!!createRes.data.id).toBe(true);
    let deleteRes = await parrotRequest(`${APIBase}/productProperties/${createRes.data.id}`, {
      method: 'DELETE',
      body: {
        loginToken
      }
    });
    expect(deleteRes.status).toBe(204);
    expect(deleteRes.success).toBe(true);
  });

});
