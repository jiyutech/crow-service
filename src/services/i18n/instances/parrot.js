import parrotRequest from '../../../request/instances/parrot'

export default function factory( baseUrl = '/api' ){
  return {

    fetchSupportedLanguages: async function() {
      return parrotRequest(`${baseUrl}/languages`).then((res)=>{
        if ( res && res.data ) {
          // => res.data = [
          //   {
          //     "name": "中文简体",
          //     "locale": "zh-Hans"
          //   }
          // ]
          res.data = res.data.map(src=>{
            return {
              name: src.name,
              lang: src.lag || src.lang,
            };
          });
          // <= res.data = [
          //   {
          //     "name": "中文简体",
          //     "lang": "zh-Hans"
          //   }
          // ]
        }
        return res;
      });
    },

    fetchWordsMap: async function(lang) {
      return parrotRequest(`${baseUrl}/words/listAll`, { body: {lang} }).then((res)=>{
        if ( res && res.data ) {
          // => res.data = [
          //   {
          //     "id": 100045,
          //     "createdDate": 19299599694,
          //     "modifiedDate": 19299599694,
          //     "key": "title",
          //     "values": [
          //       {
          //         "value": "标题",
          //         "lang": "zh_CN"
          //       }
          //     ]
          //   }
          // ]
          let map = {};
          res.data.forEach(src => {
            map[src.key] = (src.values.filter(v=>v.lang === lang)[0] || {}).value;
          });
          res.data = map;
          // <= res.data = {
          //   "key": "文案",
          //   "key": "文案",
          //   ...
          // }
        }
        return res;
      });
    }

  }
}
