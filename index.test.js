import intro from './index';

test('parrotRequest should be a function', () => {
  expect(typeof intro.parrotRequest).toBe('function');
});
